#
# multistage build file to compile ffmpeg and move its binaries
# into a smaller container
# compilation is based on this guide:
# https://trac.ffmpeg.org/wiki/CompilationGuide/Ubuntu
# the advantage of a multibuild process is that we dont have to worry about layer size
# instead we can reduce the size of the last image drastically.

FROM debian:stable

# set some env vars for the setup process
ENV BUILD_DEPS "autoconf automake build-essential cmake git-core libass-dev libfreetype6-dev libmp3lame-dev libopus-dev libtheora-dev libtool libvorbis-dev libvpx-dev mercurial pkg-config texinfo wget wget yasm zlib1g-dev libx264-dev libx265-dev libnuma-dev"
ENV BIN_DIR "/ffmpeg_bin"
ENV BUILD_DIR "/ffmpeg_build"
ENV SOURCE_DIR "/ffmpeg_sources"

# setup base environment
RUN mkdir ${BIN_DIR} ${BUILD_DIR} ${SOURCE_DIR}
# install build dependencies
RUN apt-get update \
  && apt-get install -y ${BUILD_DEPS}
WORKDIR ${SOURCE_DIR}

# compile nasm as debian still does not provide a version >=2.13
ENV NASM_VERSION=2.14.02
RUN wget https://www.nasm.us/pub/nasm/releasebuilds/${NASM_VERSION}/nasm-${NASM_VERSION}.tar.bz2 \
  && tar xjvf nasm-${NASM_VERSION}.tar.bz2 \
  && cd nasm-${NASM_VERSION} \
  && ./autogen.sh \
  && PATH="$BIN_DIR:$PATH" ./configure --prefix="$BUILD_DIR" --bindir="$BIN_DIR" \
  && make \
  && make install 

# compile libfdk-aac as its available in the debian repo
RUN git -C fdk-aac pull 2> /dev/null || git clone --depth 1 https://github.com/mstorsjo/fdk-aac \
  && cd fdk-aac \
  && autoreconf -fiv \
  && ./configure --prefix="$BUILD_DIR" --disable-shared \
  && make \
  && make install

# compile libaom (this step may fail)
RUN git -C aom pull 2> /dev/null || git clone --depth 1 https://aomedia.googlesource.com/aom \
  && mkdir -p aom_build \
  && cd aom_build \
  && PATH="$BIN_DIR:$PATH" cmake -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX="$BUILD_DIR" -DENABLE_SHARED=off -DENABLE_NASM=on ../aom \
  && PATH="$BIN_DIR:$PATH" make \
  && make install

# download and compile ffmpeg
RUN wget -O ffmpeg-snapshot.tar.bz2 https://ffmpeg.org/releases/ffmpeg-snapshot.tar.bz2 \
  && tar xjvf ffmpeg-snapshot.tar.bz2 \
  && cd ffmpeg \
  && PATH="$BIN_DIR:$PATH" PKG_CONFIG_PATH="$BUILD_DIR/lib/pkgconfig" ./configure \
      --prefix="$BUILD_DIR" \
      --pkg-config-flags="--static" \
      --extra-cflags="-I$BUILD_DIR/include" \
      --extra-ldflags="-L$BUILD_DIR/lib" \
      --extra-libs="-lpthread -lm" \
      --bindir="$BIN_DIR" \
      --enable-gpl \
      --enable-libaom \
      --enable-libass \
      --enable-libfdk-aac \
      --enable-libfreetype \
      --enable-libmp3lame \
      --enable-libopus \
      --enable-libtheora \
      --enable-libvorbis \
      --enable-libvpx \
      --enable-libx264 \
      --enable-libx265 \
      --enable-nonfree \
  && PATH="$BIN_DIR:$PATH" make \
  && make install \
  && hash -r

# use a second image to only store the required libraries and
# the ffmpeg binaries
FROM debian:stable-slim

COPY --from=0 /ffmpeg_bin/ffmpeg /usr/local/bin
COPY --from=0 /ffmpeg_bin/ffprobe /usr/local/bin

RUN apt-get update \
  && apt-get install -y libass9 libvpx5 libmp3lame0 libopus0 libtheora0 \
      libvorbis0a libx264-155 libx265-165 libvorbisenc2 \
  && rm -rf /var/lib/apt/lists/*
  
ENTRYPOINT [ "/usr/local/bin/ffmpeg" ]
